//I got help from my friend to get this code from this website to work https://www.codejava.net/coding/how-to-code-login-and-logout-with-java-servlet-jsp-and-mysql
package com.example.cit360final;


import java.sql.*;

public class UserDAO {

    public Users checkLogin(String email, String password) throws SQLException,
            ClassNotFoundException {
        String jdbcURL = "jdbc:mysql://localhost:3306/final";
        String dbUser = "root";
        String dbPassword = "Password";

        Class.forName("com.mysql.jdbc.Driver");
        Connection connection = DriverManager.getConnection(jdbcURL, dbUser, dbPassword);
        String sql = "SELECT * FROM users WHERE email = ? and password = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, email);
        statement.setString(2, password);

        ResultSet result = statement.executeQuery();

        Users user = null;

        if (result.next()) {
            user = new Users();
            user.setFirstName(result.getString("firstName"));
            user.setLastName(result.getString("lastName"));
            user.setEmail(email);
        }

        connection.close();

        return user;
    }
}
